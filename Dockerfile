FROM node

WORKDIR /app

COPY package.json /app
RUN npm install

COPY . /app

ENV NODE_ENV production
ENV PORT 4200
EXPOSE 4200

CMD ["npm", "start"]
