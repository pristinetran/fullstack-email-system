<img width="150" src="https://i.cloudup.com/zfY6lL7eFa-3000x3000.png" />
<img width="50" src="https://angular.io/resources/images/logos/angular2/angular.svg" />

## Angular2 Express Starter ( Advanced )

- Angular 4.x ( Angular2 compatible )
- ExpressJS ( 4.x - with compression )
- Webpack ( angular-cli )

[![Deploy](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

*credits*: vladotesanovic, Pristine Phuc Tran

## Concepts

- Redux ( NgRx/Store - with server calls)
- Smart & dumb components
- AOT: Ahead-of-Time compilation
- Advanced routing ( lazy loading, router outlets...)

## Contact

**Pristine Phuc Tran**     

**Email: PristineKallio@gmail.com**


## Install / Development

```bash
git clone
cd fullstack-email-system

# Install dependencies
npm install

# start server
npm run start

# Client url: http://localhost:4200
# Application ( epxress ) API: http://localhost:4300
```

Install Redux DevTools chrome extenstion:

https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd

## Build / Production

```bash

npm run build

## Deploy dist folder to app server

Structure of dist folder:

/dist/server <-- expressjs
/dist/src <-- angular2

```

## Todo

- Integrate SCSS tranpiler (for development with Bootstrap, SemanticUI,etc.)

- SWAGGER docs for listing of API endpoints
 
- Provide branches for different implementation of Mongoose and Sequelize to facilitate projects with different databases.

(RDBMS vs NoSQL)


