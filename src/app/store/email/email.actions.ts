export const EMAIL_LIST_GET = 'Email: get email list';
export const EMAIL_LIST_GET_SUCCESS = 'Email: get email list success';
export const EMAIL_LIST_GET_FAIL = 'Email: get email list fail';

export const EMAIL_ITEM_GET = 'Email: Item: get email item';
export const EMAIL_ITEM_GET_SUCCESS = 'Email: Item: get email item success';
export const EMAIL_ITEM_GET_FAIL = 'Email: Item: get email item fail';
