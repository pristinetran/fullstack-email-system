import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Store} from "@ngrx/store";
import {IAppState} from "../../store/index";
import {EMAIL_ITEM_GET} from "../../store/email/email.actions";
import {Observable} from "rxjs";
import {IEmail} from "../../store/email/email.reducer";

@Component({
  selector: 'app-email-item',
  templateUrl: './email-item.component.html',
  styleUrls: ['./email-item.component.css']
})
export class EmailItemComponent implements OnInit {
  EmailItem$: Observable<IEmail>;

  constructor(public store: Store<IAppState>, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    console.log(this.route.snapshot.params);
    this.getEmailItem(this.route.snapshot.params['_id']);
    this.EmailItem$ = this.store.select('email');
    console.log('EmailItem$', this.EmailItem$);
  }

  getEmailItem(_id): void {
    this.store.dispatch({
      type: EMAIL_ITEM_GET,
      payload: {
        _id
        // insert _token here
      }
    });
  }
}
