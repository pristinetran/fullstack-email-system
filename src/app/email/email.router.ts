import { RouterModule, Route } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import {EmailListingComponent} from "./email-listing/email-listing.component";
import {EmailItemComponent} from "./email-item/email-item.component";

const routes: Route[] = [
  {
    path: '',
    component: EmailListingComponent
  }, {
    path: 'list',
    component: EmailListingComponent
  }, {
    path: 'item/:_id',
    component: EmailItemComponent
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
